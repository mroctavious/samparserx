#!/usr/bin/perl

####Que hace el programa?#####



#####Important Info###################
#  Prefix for variables:
#	  F_*= File
#  	  a_*= Array
#	  A_*= Matrix
#	  i_*= Int number
#	  s_*= String	
#
#
#
#  SINTAX:
#	  perl SamParser.pl file_1 out_1
############################################

use 5.010;
use strict;
use warnings;
use threads;
use threads::shared;
use Thread::Queue qw( );
use IO::Handle;

##Capturamos los argumentos
my ($arg1, $arg2, $arg3, $arg4, $arg5) = @ARGV;
my $aux_pos=1;

                                                           
##Abrimos el archivo
open my $FILE1, '<', $arg1;

##Abrimos archivo nuevo
`touch $arg2`;

my $insertionsF="$arg2-Insertions";
`touch $insertionsF`;

##Abrimos archivo nuevo para deletions
my $deletionsF="$arg2-Deletions";
`touch $deletionsF`;

##Abrimos archivo nuevo para Splitreads
my $SplitReads="$arg2-SplitRead";
`touch $SplitReads`;

##Abrimos archivo nuevo para almacenar los indels
my $insdelF="$arg2-InDels";
`touch $insdelF`;

##Creamos y abrimos archivo nuevo para el log de errores
my $ERRORS="$arg2-Error.log";
`touch $ERRORS`;

#------>#Variables Globales#<--------#

##Tamaño del genom
my $GENOME_LENGTH=16300;

##Log de errores o lineas no procesadas
my $error_log="";

##Arreglo para captura de read
my @a_read;
##Arreglo para el cigar
my @a_cigar;
##Posicion de la posicion
my $i_read_pos=3;

##Posicion del CIGAR
my $i_cigar_pos=5;

##Posicion de la secuencia
my $i_sequence_pos=9;

##Variable para calcular la posicion en el genoma de referencia
my $i_last_POS;

##Variables que se compartira con los threads
##$num tendra el numero de lineas ya finalizadas
my $num:shared;
##$max es el numero de lineas del archivo SAM
#my $max=`wc -l $arg1`;
#my @max = split / /, $max;
my $max;
#$max=$max[0];
##hilo que llevara el porcentaje
#my ($thr) = threads->create(\&tiempo);
#$num=0;

# °_° ###Hashes###  °_° #

##Hash principal donde guardara los caculos echos con el cigar
my %hash :shared;

##Hash que reportara las Insertions
my %hashInsertions :shared;

##Este reporta las deletions
my %hashDeletions :shared;

##SplitR Reportara los splitreads encontrados
my %hashSplitR;

##Hash donde guardara los INDELS
my %hashInDels :shared;

##Va contando cuantas subllaves tiene esa posicion
my $i_pos;
##Posicion y Accion EJ:: 182M
my $s_key;
##Secuencia del current CIGAR
my $s_value;

#Lo mismo que lo anterior(POS KEY SEQ) pero aplicado a INSERTIONS
my $s_Insertion_key;
my $s_Insertion_value;
my $i_Insertion_pos;


##Generador de llavez
my $s_key_generator;

##Contador de total de inserciones
my $contador_inserciones=0;
##Contador de total de deleciones
my $contador_deletions=0;
##Contador de total de reads
my $cuentaLinea=0;

##Longitud de secuencias de Inseciones
my $i_num_seq_ins=0;

##Longitud de secuencias de Deleciones
my $i_num_seq_del=0;

##How many split reads found
my $i_split_read_number=0;

##How many indels reads found
my $i_indel_read_number=0;

##This variables will get the maximum length of each type of mutation
my $MAX_ins=0;
my $MAX_del=0;
my $MAX_spltrd=0;

##Get the max and min length of all the SplitReads
my $maxSpltrd=1000;
my $minSpltrd=1;

##Pequeña funcion para imprimir y esperar n segundos)
sub debug 
{
	say "########START##########";
	say $_[0];
	say "#########END##########";
	sleep(1);
}

##Funcion que ira mostrando el porcentaje del archivo
sub tiempo
{
    my $contador=0;
    my @line=("\|", "/","-", "\\", "\|", "/", "-", "\\");
    while ($num < $max-10)
    {
	#print $num;
	my $porcentaje=($num*100)/$max;
	my $rounded = sprintf "%.2f", $porcentaje;  # rounded to 2 decimal places (0.67)
	STDOUT->printflush("\rWorking -> $rounded%  [$line[$contador]]");
	sleep 1;
	if($contador == 7)
	{
		$contador=0;
	}
	else
	{
		$contador+=1;
	}

	
    }
        return ("Done");
}

###Funcion que verificara si el formato del cigar esta correcto
sub is_cigar
{

        my @a_cig=@_;
	if($a_cig[1])
	{
		##Si se encuentran los valores que deben ir
	        if( $a_cig[0] =~ /[0-9]+/ && $a_cig[1] =~ /[IDMXNS=]/)
	        {
	                return 1;
	        }
	        else
	        {
	                return 0;
	        }
	}
	else
	{
		return 0;
	}
}



#Esta funcion guarda en un hash los matches y mismatches
sub mapping_om
{	
	##Va a crear llave unica para el hash 
	$s_key_generator=$s_key.'|'.$s_value;
	##Si ya fue encontrado previamente
	if(exists $hash{$s_key_generator})
	{
		$hash{$s_key_generator}+=1;
	}
	else
	{
		$hash{$s_key_generator}=1;
	}

	
}

#Funcion que guarda en el hash de Inserciones
sub mapping_Insertions
{
	my $s_insertion_key=$_[0];
	my $s_insertion_value=$_[1];
	
	##Va a crear llave unica para el hash 
	my $s_key_generator=$s_insertion_key.'|'.$s_insertion_value;
	
	##Si ya fue encontrado previamente
	if(exists $hashInsertions{$s_key_generator})
	{
		$hashInsertions{$s_key_generator}+=1;
	}
	else
	{
		$hashInsertions{$s_key_generator}=1;
	}
	my $MAX_ins=0;

}
#Funcion que guarda las deleciones en un hash
sub mapping_Deletions
{
	##Se debe contar las deleciones de que posicion iniciarion y que posicion termino + seq
	if($_[3] == 1)
	{
		my @dif=diferencia($_[0],$_[1]);
		my $endpos=$dif[1];
		my $delkey="$_[0] | $endpos | $_[2]";
		if(exists $hashDeletions{$delkey})
		{
			#say "PKO $_[0]";
			$hashDeletions{$delkey}+=1
		}
		else
		{
			$hashDeletions{$delkey}=1;
		}
	}
	else
	{
		my $endpos=$_[0]+$_[1]-1;
		my $delkey="$_[0] | $endpos | $_[2]";
		if(exists $hashDeletions{$delkey})
		{
			#say "PKO $_[0]";
			$hashDeletions{$delkey}+=1
		}
		else
		{
			$hashDeletions{$delkey}=1;
		}
	}
	my $MAX_del=0;
}


#Funcion que guarda las splitreads en un hash
sub mapping_SplitReads
{
	##Arg0=Last position
	##Arg1=Splitread length
	##Arg2=Identifier
	##Args3=Flag
	my $splt_length=$_[1];
	
	if ( $splt_length > $maxSpltrd )
	{
		$maxSpltrd=$splt_length;
	}
	
	if ( $splt_length < $minSpltrd )
	{
		$minSpltrd=$splt_length;
	}
	
	##Se debe contar las deleciones de que posicion iniciarion y que posicion termino + seq
	if($_[3] == 1) ##1 is the flag, if it is activated it means that deletion will have to start from 0 because it went out of range
	{
		my @dif=diferencia($_[0],$_[1]);
		my $endpos=$dif[1];
		my $delkey="$_[0] | $endpos | $_[2]";
		if(exists $hashSplitR{$delkey})
		{
			$hashSplitR{$delkey}+=1
		}
		else
		{
			$hashSplitR{$delkey}=1;
		}
	}
	else
	{
		my $endpos=$_[0]+$_[1]-1;
		my $delkey="$_[0] | $endpos | $_[2]";
		if(exists $hashSplitR{$delkey})
		{
			$hashSplitR{$delkey}+=1
		}
		else
		{
			$hashSplitR{$delkey}=1;
		}
	}
}


#Funcion que guarda las deleciones en un hash
sub mapping_InsDel
{
	##$_[3] Verifica si se sale del genoma
	##$_[4] Es la secuencia insertada
	##Se debe contar las deleciones de que posicion iniciarion y que posicion termino + seq
	if($_[3] == 1)
	{
		my @dif=diferencia($_[0],$_[1]);
		my $endpos=$dif[1];
		my $delkey="$_[0] | $endpos | ID | $_[4]";
		if(exists $hashInDels{$delkey})
		{
			$hashInDels{$delkey}+=1
		}
		else
		{
			$hashInDels{$delkey}=1;
		}
	}
	else
	{
		my $endpos=$_[0]+$_[1]-1;
		my $delkey="$_[0] | $endpos | ID | $_[4]";
		if(exists $hashInDels{$delkey})
		{
			$hashInDels{$delkey}+=1
		}
		else
		{
			$hashInDels{$delkey}=1;
		}
	}
	
}

sub ancla_izq
{
	my($i_in,$ipos)= @_;
	if($i_in >= $ipos)
	{
		return 1;
	}
	else
	{
		return 0
	}
}

sub ancla_derecha
{
	my ($for_i,$sum,@cigar_ref)=@_;
	my @tmp_ancla=@cigar_ref;
	
	##say "Cigar:@tmp_ancla\tSuma:$sum\tFor_i=$for_i";

	##Caso base, que supere el ancla
	if($sum > 9)
	{
		return 1;
	}
	##De lo contrario va a recorrer recursivamente el cigar en busca de la ancla final
	else
	{
		if($tmp_ancla[$for_i+2] && ($tmp_ancla[$for_i+3] eq "M" || $tmp_ancla[$for_i+3] eq "X" || $tmp_ancla[$for_i+3] eq "=" || $tmp_ancla[$for_i+3] eq "D"))
		{
			return ancla_derecha($for_i+2, $sum+$tmp_ancla[$for_i+2],@tmp_ancla)
		}
		##Si todavia hay mas cigar, entonces seguir explorando
		elsif($tmp_ancla[$for_i+2])
		{
			return ancla_derecha($for_i+2, $sum,@tmp_ancla)
		}
		##Si ya no hay mas cigar y no se cumplio la condicion del ancla, entonces regresa 0
		else
		{
			return 0;
		}		
	}
}

##Funcion para calcular el CIGAR de cada read
sub SAMextractor
{
	my $err=0;
    #my ($thr) = threads->create(\&tiempo);
    ##Mientras lea el archivo SAM
    while(<$FILE1>)
    {
    		#say "Reading new line->";
		chomp;
		##Si es parte de un header, entonces sigue con la siguiente linea
		if ($_ =~ /^\s*@/)
		{
			#$num = shared_clone($num+1);
			next;
		}
		
		##Separo por [TABS]
		@a_read = split "\t", $_;
		chomp (@a_read);
		##Separo por letras y numeros y hace el CIGAR. La posicion par es el numero y la posicion impar es la letra
		if( $a_read[$i_cigar_pos] !~ /[*]/)
		{
			@a_cigar = split( /(?<=\d)(?=\D)|(?<=\D)(?=\d)/, $a_read[$i_cigar_pos] );
			if($a_read[$i_read_pos] <= 16300)
			{
				#debug($a_read[$i_read_pos]);
				#say "It is lesser than 16300, doing now MisMatchIntervalCalc";
				MisMatchIntervalCalculator();
				#say "MisMatchInterval have done now";
			}
		
     		}
     		$cuentaLinea+=1;
		#say $cuentaLinea;

	}

}

##Funcion para ordenar por posicion
sub by_pos
{
	
	my ($posA, $seqA) = split(/[IMX=]\|/,$a);
	my ($posB, $seqB) = split(/[IMX=]\|/,$b);
	return $posA <=> $posB;
	
}

##funcion que ordena el hash dependiendo de las repeticion
sub by_values
{
	
	my ($posA, $seqA) = split(/ \| /,$a);
	my ($posB, $seqB) = split(/ \| /,$b);
	return $posA <=> $posB;
	
}

sub error_report
{
	open my $F_Errores, '>', $ERRORS;
	print $F_Errores "#The following lines were not in correct format:$error_log";
	close $F_Errores;
}

##Ordena el hash principal en el que se guardan los matches y mismatches
sub hashsort
{
	open my $F_SALIDA, '>', $_[0];
	lock %hash;
	foreach my $key (sort by_pos keys %hash)
	{
		my ($pos, $seq) = split(/\|/,$key); 
		print $F_SALIDA "$pos$hash{$key} | $seq\n";
	}
	close $F_SALIDA;
}

##Ordena el hash de inserciones y crea archivos
sub hashsort_Insertion
{
	open my $F_INSERTIONS, '>', $_[0];
	lock %hashInsertions;
	print $F_INSERTIONS "\@Insertions=$contador_inserciones - Reads=$cuentaLinea - Avg_Lenght=$_[3]\n";
	foreach my $insertion_key (sort by_pos keys %hashInsertions)
	{
		my ($pos, $seq) = split(/\|/,$insertion_key);
		print $F_INSERTIONS "$pos$hashInsertions{$insertion_key} | $seq\n";
	}
	close $F_INSERTIONS;

}

##Se ordenan las deleciones y se crea el archivo
sub hashsort_Deletions
{
	open my $F_DELETIONS, '>', $_[0];
	print $F_DELETIONS "\@Deletions=$_[1] - Total Reads=$_[2]\n";
	lock %hashDeletions;
	foreach my $deletion_key (sort { $hashDeletions{$b} <=> $hashDeletions{$a}}(keys %hashDeletions))
	{
		print $F_DELETIONS "$deletion_key | $hashDeletions{$deletion_key}\n";
	}
	close $F_DELETIONS;
	return 1;
}


##Se ordenan los splitreads y se crea el archivo
sub hashsort_SplitReads
{
	open my $F_Splitreads, '>', $_[0];
	
	if($maxSpltrd<=$minSpltrd)
	{
		##Imprime los valores maximos de los splitreads $maxSpltrd
		print $F_Splitreads "\#:0:$minSpltrd:$_[1]\n";
	}
	else
	{
		##Imprime los valores maximos de los splitreads $maxSpltrd
		print $F_Splitreads "\#:$minSpltrd:$maxSpltrd:$_[1]\n";
	}
	##Imprime un pequeño header sobre informacion relevante del analisis
	print $F_Splitreads "\@SplitReads=$_[1] - Total Reads=$_[2]\n";
	#lock %hashSplitR;
	foreach my $sp_key (sort { $hashSplitR{$b} <=> $hashSplitR{$a}}(keys %hashSplitR))
	{
		print $F_Splitreads "$sp_key | $hashSplitR{$sp_key}\n";
	}
	close $F_Splitreads;
	return 1;
}


##Se ordenan los indels y se crea el archivo
sub hashsort_InDels
{
	open my $F_InDels, '>', $_[0];
	##Imprime un pequeño header sobre informacion relevante del analisis
	print $F_InDels "\@Number of INDELS=$_[1] - Total of Reads=$_[2]\n";
	lock %hashInDels;
	foreach my $sp_key (sort { $hashInDels{$b} <=> $hashInDels{$a}}(keys %hashInDels))
	{
		my @tmp_indels=split /\|/, $sp_key;
		print $F_InDels "$tmp_indels[0] | $tmp_indels[1] | $tmp_indels[2] | $hashInDels{$sp_key} | $tmp_indels[3]\n";
	}
	close $F_InDels;
	return 1;
}

##This function will print the maximum value of the mutations found
sub print_max_values
{
	my $out=$_[0];
	my $spltrd_total=$_[1];
	my $min=$_[2];
	my $max=$_[3];
	open my $F_MAX, '>', "$out";
	print $F_MAX "$spltrd_total:$min:$max\n";
	close $F_MAX;
}


##Esta funcion es la principal, Recorre todo el CIGAR y dependiendo de su mutacion hace diferentes cosas
sub MisMatchIntervalCalculator
{
	##Tamaño del CIGAR
	my $i_cigar_size=@a_cigar;
	##Se guarda la posicion inicial
	my $i_last_POS=$a_read[$i_read_pos];
	##Se guarda la posicion inicial
	my $i_init_POS=$a_read[$i_read_pos];
	my $substring;
	my $i_sequence_position=0;
	my $flag=0;
	##Se recorre todo el CIGAR y se analiza
	#say "Recorriendo el CIGAR->";
	for(my $i=0; $i<$i_cigar_size; $i+=2)
	{
		my $time = localtime;
		#say "Analizando $a_cigar[$i]$a_cigar[$i+1]";
		if($flag==1)
		{
			#say "$time  Bandera activada, saltando iteracion";
			$flag=0;
			next;
		}
		
		##Activator verificara si se vuelve a regresar a la primera posicion del genoma
		my $activator=0;
		if($a_cigar[$i]+$i_last_POS > 16300)
		{
			$activator=1;
		}
		if($a_cigar[$i+1] eq "M" || $a_cigar[$i+1] eq "X" || $a_cigar[$i+1] eq "=")
		{
			#say "Its M -> $time";
			$substring=substr $a_read[$i_sequence_pos], $i_sequence_position, $a_cigar[$i];
			$i_pos=$i_last_POS;
			$s_key="$i_last_POS$a_cigar[$i+1]";
			$s_value=$substring;
			
			##Toma en cuenta la posicion e imprime donde encontro el Match o Mismatch
			#say "Mapping M -> $time";
			mapping_om();
			#say "Mapping M DONE -> $time";
			$i_last_POS+=$a_cigar[$i];
			
			##Se suma a la posicion de la secuencia
			$i_sequence_position+=$a_cigar[$i];
		}	
	
		elsif($a_cigar[$i+1] eq "I")
		{
			#say "Its I -> $time";
			##Hago el substring
			$substring=substr $a_read[$i_sequence_pos], $i_sequence_position, $a_cigar[$i];
			
			##Se aumenta el contador de inserciones
			$contador_inserciones+=1;
			
			##Se consigue la pasicion donde empieza la insercion
			$i_Insertion_pos=$i_last_POS;
			$i_sequence_position+=$a_cigar[$i];
			
			##Se genera llave para almacenar en hash
			my $s_Insertion_key="$i_last_POS$a_cigar[$i+1]";
			my $s_Insertion_value=$substring;
			
			##Verifica que no haya ocurrido un INDEL
			my $indel=0;
			
			##Verificar si es un INDEL
			if($i_cigar_size>$i+2)
			{
				if($a_cigar[$i+3] eq "N")
				{
					#say "Its INDEL -> $time";

					##say "Posible candidato $_";
					##Ancla izquierda
					my $i_init = $i_last_POS-10;
			
					##Ancla derecha
					my $i_end = $i_last_POS+10;
					
					#say "Calculando Ancla $time";
					my $anc_izq=ancla_izq($i_init,$i_init_POS);
					my $anc_der=ancla_derecha($i+2, 0,@a_cigar);
					#say "Ancla calcuada $time";
					#debug("IZQ:$anc_izq  DER:$anc_der   INDELS");
					##Si cumple con la regla de las anclas
					##if( $i_init >= $i_init_POS && $i_end < $i_tamano_final_read)
					if( $anc_izq+$anc_der==2)
					{
						#debug("ENTRO al Mapping Indels");
						#Se mappea al hash de deleciones pero se marca como un SplitRead
						#say "N -> $i_init >= $i_init_POS || $i_end < $i_tamano_final_read";
						#say "Mapping INDEL $time";
						mapping_InsDel($i_last_POS, $a_cigar[$i+2], $a_cigar[$i+3], $activator,$s_Insertion_value);
						##Si esta dentro del ancla entonces se considera como SR por lo tanto se cuenta en el calculo del CIGAR
						$i_last_POS+=$a_cigar[$i];
						$i_num_seq_del+=$a_cigar[$i];
						$i_indel_read_number+=1;
						$indel=1;
						#say "Mapping INDEL DONE $time";
						$flag=1;
							
					}
					if($activator==1)
					{
						$i_last_POS=1;	
					}
				}
			}
			if($indel==0)
			{
				##Se aumenta el contador de inserciones
				$i_num_seq_ins+=$a_cigar[$i];
				#say "Its I and not INDEL and will be mapped now $time";
				##Se manda a guardar
				mapping_Insertions($s_Insertion_key, $s_Insertion_value);
				if ($MAX_ins < $a_cigar[$i])
				{
					$MAX_ins=$a_cigar[$i];	
				}

				#say "Mapping I DONE! $time";
			
			}
			
		}
		elsif($a_cigar[$i+1] eq "D")
		{
			##Se suma el numero de deleciones al contador de deleciones
			$contador_deletions+=$a_cigar[$i];
			##Se llama la funcion que guarda la delecion en hash
			mapping_Deletions($i_last_POS,$a_cigar[$i], $a_cigar[$i+1], $activator);
			##La ultima posicion en el genoma se le suma el numero de deleciones que tiene el read
			$i_last_POS+=$a_cigar[$i];
			##$i_num_seq_del es el numero de inseciones, y sirve para imprimir algunos resultados en el archivo
			$i_num_seq_del+=$a_cigar[$i];
			if ($MAX_del < $a_cigar[$i])
			{
				$MAX_del=$a_cigar[$i];	
			}
			
		
		}
		elsif($a_cigar[$i+1] eq "N")
		{

			##Ancla izquierda
			my $i_init = $i_last_POS-10;
			
			##Ancla derecha
			my $i_end = $i_last_POS+15;
			#say "Calculando Ancla $time";

			my $anc_izq=ancla_izq($i_init,$i_init_POS);
			my $anc_der=ancla_derecha($i+2, 0,@a_cigar);
			#say "Ancla calculada $time";
			#debug("IZQ:$anc_izq  DER:$anc_der SPLITREADS");                                        		
			##Si cumple con la regla de las anclas
			if( $anc_izq+$anc_der==2)
			{
				if($a_cigar[$i] < ($GENOME_LENGTH / 2))
				{
					#Se mappea al hash de deleciones pero se marca como un SplitRead
					#say "N -> $i_init >= $i_init_POS || $i_end < $i_tamano_final_read";
					#say "Enviando SplitRead a mapping $time";
					mapping_SplitReads($i_last_POS, $a_cigar[$i], $a_cigar[$i+1], $activator);
					#say "Mapping done $time";
					##Si esta dentro del ancla entonces se considera como SR por lo tanto se cuenta en el calculo del CIGAR
					$i_last_POS+=$a_cigar[$i];
					$i_num_seq_del+=$a_cigar[$i];
					$i_split_read_number+=1;
					
					if ($MAX_spltrd < $a_cigar[$i])
					{
						$MAX_spltrd=$a_cigar[$i];
					}
				}
				
			}
		}
	
		else
		{
			##Si toma en cuenta su posicion
			$i_last_POS+=$a_cigar[$i]-1;
			##Se suma a la posicion de la secuencia
			$i_sequence_position+=$a_cigar[$i];			
		}
		if($activator==1)
		{
			$i_last_POS=1;	
		}
		
	}
}

##Funcion que analiza si la sequencia se sale del 16300
sub pos_analisis
{
	if($_[0]+$_[1] > 16300)
	{
		return 1;	
	}
	else
	{
		return 0;
	}
}

##Funcion que devuelve el nuevo rango de posiciones.
##Es para cuando se sale de la posicion 16300
sub diferencia
{
	##ARG0: Ultima posicion en el genoma
	##ARG1: Cuanto avanzara
	my $pos=$_[0];
	my $avanzar=$_[1];
	my $suma=$pos+$avanzar;
	my $dif_total=$suma-16300;
	my $dif_hasta_total=$avanzar-$dif_total;
	my @return=($dif_hasta_total,$dif_total);
	return @return;
}


###Si esta bien la sintaxis entonces que entre al programa
if ($arg1 && $arg2)
{
	##Empieza el programa
	SAMextractor();
	##Se consigue promedio de las delecion y tambien para inserciones
	my $avg_del=$i_num_seq_del/$contador_deletions;
	my $avg_ins=$i_num_seq_ins/$contador_inserciones;
	
	##Se envian a ordenar los hashes usando hilos
	threads->new(\&hashsort,$arg2);
	threads->new(\&hashsort_Deletions,$deletionsF, $contador_deletions, $cuentaLinea);
	threads->new(\&hashsort_InDels,$insdelF,$i_indel_read_number, $cuentaLinea);
	threads->new(\&hashsort_Insertion,$insertionsF, $contador_inserciones, $cuentaLinea,$avg_ins);
	threads->new(\&error_report,$error_log);
	my $splt_num=$contador_deletions+$i_split_read_number;

	threads->new(\&print_max_values,"$arg2.max",$splt_num,$minSpltrd,$maxSpltrd);
	hashsort_SplitReads($SplitReads, $i_split_read_number, $cuentaLinea);
	##Esperamos a que terminen los hilos
	$_->join() for threads->list();
	close $FILE1;
}



##De lo contrario que muestre la informacion de ayuda
else
{
	say "Syntax error, please use it like this:\n\tperl SamParser.pl File.sam\n\nExample: perl SamParser.pl File.sam Output.txt";
}

#say "Errores: $error_log";

