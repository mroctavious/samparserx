#!/usr/bin/perl

use 5.010;
use strict;
use warnings;
use threads;
use lib qw(include);
use Formulas;

##Copiamos los argumentos para manipularlos despues
#my ($arg1, $arg2, $arg3, $arg4, $arg5) = @ARGV;
my @args=@ARGV;
#say $#ARGV;
my $contador=0;

#Hash de la tabla
my %hash;

##Datos que se usaran para la formula
my $average=50;
my $total_reads=0;
my $GENOME=16300;

##Agregamos las formulas
#use 'include/formulas.pm';
##Como se ordenaran las llaves
sub by_pos
{
	
	my ($posA, $seqA) = split(/\|/,$a);
	my ($posB, $seqB) = split(/\|/,$b);
	return $posA <=> $posB;
}

##Conseguir opcion o archivo
sub options
{
	my $input=$_[0];
	if ( $input eq "-del" )
	{
		return 1;
	}
	elsif( $input eq "-split" )
	{
		return 2;
	}
	elsif( $input eq "-insertion" )
	{
		return 3;
	}
	elsif( $input eq "-indel" )
	{
		return 4;
	}
	else
	{
		return 0;
	}
}

##Se recorreran los archivos uno por uno
my $splt_arg;
my @tmp_ARGV=@args;
my $option=options(pop(@tmp_ARGV));
if( $option != 0 )
{
	pop(@args);
}

my @args2=@args;
foreach(@args)
{
	##file es el nombre del archivo
	my $file = $_;
	##Abrimos el archivo y lo recorremos
	##Esto se hara por cada argumento dado
	open my $F_ENTRADA, '<', $file;
	
	#Recorremos cada linea de la tabla
	while(<$F_ENTRADA>)
	{
		if(! $_)
		{
			next;
		}
		##Si es comentario se ignora
		if ($_ =~ /^\s*#/)
		{
			next;
		}
		##Si es @ abstraemos la informacion necesaria
		elsif($_ =~ /^\s*@/)
		{
			##Spliteamos y abstraemos la informacion
			my @avg=split / - /, $_;
			my @splt0=split /=/, $avg[0];
			##Doble spliteamos y conseguimos el total del reads
			my @splt1=split /=/, $avg[1];
			$total_reads=$splt1[1];
			next;
		}
		chomp;
		my @pos_seq = split / \| / , $_;
		my $seq = $pos_seq[1];
		##Spliteamos entre tipo de mutacion y repeticiones
		my @pko = split /[IDSR]+/, $pos_seq[0];
		my $pos = $pko[0];
		my $repeticiones=0;
		##Usamos la formula para calcular la puntacion de cada muestra
		##If its a Deletion
		if( $option == 1 )
		{
			$repeticiones = Formulas::f_deletion($pko[1], $average, $total_reads, $GENOME );
		
		}
		##If its a SplitRead
		elsif( $option == 2 )
		{
			$repeticiones = Formulas::f_splitread($pko[1], $average, $total_reads, $GENOME );

		}
		##If its an Indels
		elsif( $option == 4 )
		{
			$repeticiones = Formulas::f_indel($pko[1], $average, $total_reads, $GENOME );

		}
		##If its an Insertion
		else
		{
			$repeticiones = Formulas::f_insertion($pko[1], $average, $total_reads, $GENOME );
		}
		
		##Si se quiere debugear quitar comentarios de abajo
		#say "SEQ:$seq   POS:$pos    REP:$pko[1]";
		#say "ANTES:";
		#say "POS: $pos    REP: $pko[1]      SEQ: $seq";
		#say "DESPUES";
		#say "POS: $pos    REP: $repeticiones      SEQ: $seq";
		#sleep(2);
		##Generamos la llave para el hash
		my $key_generator="$pos|$seq";
		##Checar si existe
		if(exists $hash{$key_generator})
		{
			##Se concatena el nombre del archivo y el hash archivo
			$hash{$key_generator}.="|$file*$repeticiones";
		}
		else
		{
			$hash{$key_generator}="$file*$repeticiones";
		}
	}
	close $F_ENTRADA;	
}

my $header="LOCUS\tSEQUENCE";
my %header_hash;
#for(my $i=0; $i<=$#ARGV+1;$i++)
my $count=2;
foreach(@args2)
{
	#say $_;
	#sleep(1);
	$header_hash{$_}=$count;
	my @file_name=split /\//, $_;
	my @samples=split /\./, $file_name[$#file_name];
	$header="$header\t$samples[0].$samples[1]";
	$count+=1;
}

say "$header";
foreach my $key (sort by_pos keys %hash)
{
	my ($pos, $seq) = split(/\|/,$key); 
	my @salida = split(/\|/,$key); 
	
	##Pongo en 0 todas las repeticiones de cada uno de los archivos
	foreach my $head_names(keys %header_hash)
	{
		$salida[$header_hash{$head_names}]=0;
		
		#say "####$head_names###";
		#say "--->$header_hash{$head_names}";
	}
	
	my @aux= split /\|/, $hash{$key};
	#say "Se Spli con |  ->   $hash{$key}\nComo resultado: 0:$aux[0]   1:$aux[1]";
	#sleep(1);
	foreach(@aux)
	{
		my ($file_name, $number) = split /\*/, $_;
		#say "FILE:$file_name    NUM:$number";
		$salida[$header_hash{$file_name}]=$number;
	}
	my $out="";

	foreach (@salida)
	{
		#say "SALIDA=$_";
		$out.="$_\t";
		#sleep(1);
	}
	say $out;
}


