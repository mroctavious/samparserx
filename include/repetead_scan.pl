#!/usr/bin/perl

use strict;
use 5.010;
use warnings;
use threads;
use threads::shared;
use Thread::Queue qw( );
use lib qw(include);
use Formulas;

##Argumentos
my ($genome, $arg1, $OUT, $OPTION)=@ARGV;

##Creamos archivo de salida
`touch $OUT`;

##Extencion de archivo de salida repeated 
my $temp=".repeated";
my $salida_dir_del :shared;
$salida_dir_del="$OUT/$temp";

my $GENOME_LENGTH=16300;
##Salida de los dir dels
#my $F_salida

##Abrimos archivo
open my $F_Dir_Del_Salida, '>', $OUT;

##Abrimos el Genoma de regerencia
open my $F_Genome, '<', $genome;
open my $F_entrada, '<', $arg1;

##Leemos la linea
my $GenomeSequence :shared;
$GenomeSequence=<$F_Genome>;
close $F_Genome;


##Hashes
my %Hash_Dup_Rep :shared;
my %Hash_Total_Dup_Rep :shared;

##counting variables
my $total=0;
my $maxSpltrd=0;
my $MAX_reads=0;
my $MAX_RD=15;
my $minSpltrd=$GENOME_LENGTH;

##Old header from file
my $old_header;

##Orden personalizado, se ordenara por orden de posicion
sub by_pos
{
	my ($posA_tmp, $seqA_tmp) = split(/\|/,$a);
	my @aux_pos = split(/[DS]\|/, $posA_tmp);
	my $posA=$aux_pos[0];
	my ($posB_tmp, $seqB_tmp) = split(/\|/,$b);
	@aux_pos = split(/[DS]\|/,$posB_tmp);
	my $posB=$aux_pos[0];
	return $posA <=> $posB;
}


##Formula para calcular el numero
sub formula_rd
{
	##Cuantos repeated se encontraron
	my $freq=$_[0];
	##El numero total de reads en el SAM
	
	###Formula:
	return ( ($freq*10**6*50) / (20 * $MAX_reads) );
	
}



##Funcion que calculara cuantos nucleotidos se extraeran
sub calcular_tamano
{
	my $contador=0;
	my $i=$_[0];
	my $ver=0;
	say "Contador: $contador";
	say "i=$i";
	say "Ver=$ver";
	while($ver==0)
	{
		if($i==$GENOME_LENGTH)
		{
			$i=0;
		}
		$i++;
		if($contador==$_[1])
		{
			$ver=1;
		}
		$contador++;
	}
	return $contador;
}

    ###############AQUI PONER EL CODIGO DE LOS DIRECT DELETIONS##########################

sub duplicated_deletion_backward
{
	##Posicion inicial de la delecion y posicion final del genoma
	my $pos_inicial=$_[0];

	##Posicion final de la delecion del read
	my $pos_final=$_[1];

	##Indice actual, va a ir aumentando
	my $index=$_[2];

	##String que llevara la concatenacion	
	my $string=$_[3];

	##Verificar que no se salga del rango GENOME_LENGTH
	##Conseguir posicion inicial
	my $pos_inicial_analizada=diferencia_atras_singular($pos_inicial-$index);
	##Conseguimos cuanto se avanzara para el substring
	my $pos_final_analizada=diferencia_atras_singular($pos_final-$index);
	##Checa el tamaño del string que se esta formando
	my $str_len=length($string);
	##Conseguimos el nucleotido de cada lado, delecion vs genoma de referencia
	my $going_left_gen=substr($GenomeSequence,$pos_inicial_analizada,1);
	my $going_left_del=substr($GenomeSequence,$pos_final_analizada,1);
	##Verifica que sean iguales
	if( $going_left_gen eq $going_left_del  && $index<15)
	{
		return duplicated_deletion_backward($pos_inicial, $pos_final, $index+1, "$going_left_gen$string");
	}
	else
	{
		##Si se armo alguna cadena
		if( $str_len > 0)
		{
			##Empezamos a preparar todo para guardar la cadena
			$pos_inicial_analizada=diferencia_atras_singular($pos_inicial-$index+1);
			$pos_final_analizada=diferencia_atras_singular($pos_final-$index+1);
			$str_len=length($string);
			$going_left_gen=substr($GenomeSequence,$pos_inicial_analizada,1);
			$going_left_del=substr($GenomeSequence,$pos_final_analizada,1);

			##Creamos llave para ponerla en el hash
			my $posicion_inicial=$pos_inicial_analizada;
			my $posicion_final=$pos_inicial;
			my $str_len=length($string);
			##Si ya existe entoncs agregar +1
			if(exists $Hash_Total_Dup_Rep{$str_len})
			{
				$Hash_Total_Dup_Rep{$str_len}+=1;
			}
			else
			{
				#De lo contrario se inicializa la llave
				$Hash_Total_Dup_Rep{$str_len}=1;
			}
			##Contar el numero de ocaciones donde eran de n tamaño del repetido 
			my $key="$posicion_inicial|$posicion_final|$string|$str_len|<-";
			say $key;
			if(exists $Hash_Dup_Rep{$key})
			{
				$Hash_Dup_Rep{$key}+=1;
			}
			else
			{
				$Hash_Dup_Rep{$key}=1;
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

sub duplicated_deletion_forward
{
	##Posicion inicial de la delecion y posicion final del genoma
	my $pos_inicial=$_[0];

	##Posicion final de la delecion del read
	my $pos_final=$_[1];

	##Indice actual, va a ir aumentando
	my $index=$_[2];

	##String que llevara la concatenacion	
	my $string=$_[3];

	##Verificar que no se salga del rango GENOME_LENGTH
	##Conseguir posicion inicial
	my $pos_inicial_analizada=diferencia_adelante_singular($pos_inicial+$index);
	
	##Conseguimos cuanto se avanzara para el substring
	my $pos_final_analizada=diferencia_adelante_singular($pos_final+$index);
	
	##Checa el tamaño del string que se esta formando
	my $str_len=length($string);
	
	##Conseguimos el nucleotido de cada lado, delecion vs genoma de referencia
	my $going_left_gen=substr($GenomeSequence,$pos_inicial_analizada,1);
	my $going_left_del=substr($GenomeSequence,$pos_final_analizada,1);

	##Verifica que sean iguales
	if( $going_left_gen eq $going_left_del  && $index<15)
	{
		return duplicated_deletion_forward($pos_inicial, $pos_final, $index+1, "$going_left_gen$string");
	}
	else
	{
		##Si se armo alguna cadena
		if( $str_len > 0)
		{
			##Empezamos a preparar todo para guardar la cadena
			$pos_inicial_analizada=diferencia_adelante_singular($pos_inicial-$index+1);
			$pos_final_analizada=diferencia_adelante_singular($pos_final-$index+1);
			$str_len=length($string);
			$going_left_gen=substr($GenomeSequence,$pos_inicial_analizada,1);
			$going_left_del=substr($GenomeSequence,$pos_final_analizada,1);

			##Creamos llave para ponerla en el hash
			my $posicion_inicial=$pos_inicial_analizada;
			my $posicion_final=$pos_inicial;
			my $str_len=length($string);
			
			##Si ya existe entoncs agregar +1
			if(exists $Hash_Total_Dup_Rep{$str_len})
			{
				$Hash_Total_Dup_Rep{$str_len}+=1;
			}
			else
			{
				#De lo contrario se inicializa la llave
				$Hash_Total_Dup_Rep{$str_len}=1;
			}
			##Contar el numero de ocaciones donde eran de n tamaño del repetido 
			my $key="$posicion_inicial|$posicion_final|$string|$str_len|->";
			if(exists $Hash_Dup_Rep{$key})
			{
				$Hash_Dup_Rep{$key}+=1;
			}
			else
			{
				$Hash_Dup_Rep{$key}=1;
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
}


sub imprimir_archivo
{
	my %formula_hash;
	
	#Duplicated Repeats
	my $type="RD";
	foreach my $key (sort by_pos keys %Hash_Dup_Rep)
	{
		my ($pos_inicial,$pos_final, $seq, $del_len, $direction) = split /\|/, $key;
		my $how_many=$Hash_Dup_Rep{$key};
		my $formula_result=0;
		print $F_Dir_Del_Salida "$pos_inicial$type$how_many | $seq | $del_len | $direction\n";
		if(exists $formula_hash{$del_len})
		{
			$formula_result=$how_many; ##Se convirtio en entero
			$formula_hash{$del_len}+=$formula_result;
		}
		else
		{
			$formula_result=$how_many; ##Igual aqui, no formula usada
			$formula_hash{$del_len}=$formula_result;
		
		}
		#say "Resultado Formula: $formula_result  \t cuantos se enviaron:$how_many\n";
		#sleep 1;
	}	
	
	my $total_dir_dels=0;
	##Se ordena para la imprecion del archivo
	foreach my $key (sort by_pos keys %Hash_Total_Dup_Rep)
	{
		print $F_Dir_Del_Salida "#$key=>$Hash_Total_Dup_Rep{$key}\n";
		$total_dir_dels+=$Hash_Total_Dup_Rep{$key};
	}
	print $F_Dir_Del_Salida "#T=>$total_dir_dels\n";	
	#print $F_Dir_Del_Salida "I_POS\tE_POS\tSEQUENCE\tLength\tRepeated";
	close $F_Dir_Del_Salida;	

}

##Da los rangos nuevos en caso de que se salga de la posicion $GENOME_LENGTH
sub diferencia
{
	##ARG0: Ultima posicion en el genoma
	##ARG1: Cuanto avanzara
	my $pos=$_[0];   ##16296
	my $avanzar=$_[1];  ##26
	my $suma=$pos+$avanzar;
	my $dif_total=$suma-$GENOME_LENGTH;
	my $dif_hasta_total=$avanzar-$dif_total;
	my @return=($dif_hasta_total,$dif_total);
	return @return;
}                        

##Funcion que verifica que no se alla salido del rango del genoma, 
##si se sale entonces empieza desde la posicion 16300, de lo contrario
##se queda con la posicion que tiene
sub diferencia_atras_singular
{
	my $pos=$_[0];
	if($pos<1)
	{
		return $GENOME_LENGTH+$pos;
	}
	else
	{
		return $pos;
	}
}

##Funcion que verifica que no se alla salido del rango del genoma, 
##si se sale entonces empieza desde la posicion 1, de lo contrario
##se queda con la posicion que tiene
sub diferencia_adelante_singular
{
	my $pos=$_[0];
	if($pos>$GENOME_LENGTH)
	{
		return 1;
	}
	else
	{
		return $pos;
	}
}

##Va analizar linea por linea
while(<$F_entrada>)
{
	chomp;
	##Para saltar cualquier comentario que empieze con #
	if($_ =~ /^\s*[A-Z]/)
	{
		next;	
	}
    
    	my @read;
	if($OPTION eq "-rndm")
	{
		##Aqui se separa la linea al encontrar ' | ' que es el formato del deletion
		@read = split "|", $_;	
	}
	else
	{
		@read = split "\t", $_;
	}
	my $read_length=length($read[1]); 
	say "LENGTH:$read_length";
	#say @read;
	my $start_pos=$read[0];
	say "START_POS:$start_pos";
	my $end_pos=$start_pos+$read_length;
	say "END_POS:$end_pos";
	##La variable seq (Sequence) se limpia
	my $seq="";
	my $tamano=0;

	##Si el tamaño de la delecion es mas grande que 10=> Cambio a 15
	if(abs($read_length) >= 15) ##Era 10, fue cambiado el dia 19 Abril 2017
	{
    		say "Entro para ser repeated";
    		##Se va a insertar aqui el codigo de las direct dels
		duplicated_deletion_backward($start_pos, $end_pos, 0, "");
		say "[($start_pos, $end_pos, 0, )]";
		duplicated_deletion_forward($start_pos, $end_pos, 0, "");
	}
    
	##Si llega a pasar la posicion 16300, usaremos la nueva posicion final que sera la de inicio
	#if($end_pos > 16300)
	#{
	#	say "IF: ($end_pos > 16300)";
	#	##se llaman las funciones que calcularan la diferencia y rango
    	#	$tamano=calcular_tamano($start_pos, $end_pos);
    	##	say $tamano;
	#	sleep(1);

	#}
	#else
	#{
	#	$tamano=$end_pos-$start_pos+1;
	#}
	#say $tamano;
	say "Termino iteracion";
	say "";
}

imprimir_archivo();

