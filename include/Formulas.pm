package Formulas;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(f_insertion f_deletion, f_splitread, its_deletion);

##Formula para normalizar el numero de inserciones
sub f_insertion
{
	my $insertion_number=$_[0];
	my $tmp_average=$_[1];
	my $tmp_total_reads=$_[2];
	my $tmp_genome_len=$_[3];	
	return ( ( $insertion_number * $tmp_genome_len * ( 10**6 ) ) / ( $tmp_total_reads * $tmp_average ) );
}

##Formula para normalizar el numero de deleciones encontradas
sub f_deletion
{
	my $deletion_number=$_[0];
	my $tmp_average=$_[1];
	my $tmp_total_reads=$_[2];
	my $tmp_genome_len=$_[3];
	$deletion_number=$deletion_number * (48/50);
	return ( ( ($deletion_number * $tmp_genome_len) * ( 10**6 ) ) / ( $tmp_total_reads * $tmp_average ) );
}


##Formula para las deleciones grande
sub f_splitread
{
	my $s_deletion_number=$_[0];
	my $tmp_average=$_[1];
	my $tmp_total_reads=$_[2];
	my $tmp_genome_len=$_[3]; 
	
	return ( ( $s_deletion_number * ( 2.5 * $tmp_genome_len ) * ( 10**6 ) ) / ( $tmp_total_reads * $tmp_average ) );
}

##Formula para las deleciones grande
sub f_indel
{
	my $s_deletion_number=$_[0];
	my $tmp_average=$_[1];
	my $tmp_total_reads=$_[2];
	my $tmp_genome_len=$_[3]; 
	
	return ( ( $s_deletion_number * ( 2.5 * $tmp_genome_len ) * ( 10**6 ) ) / ( $tmp_total_reads * $tmp_average ) );
}

##Funcion que multiplicara cada delecion por 48/50, es porque minimo debe haber un ancla de 1, 1 a la izq y 1 a la derecha -> 1 + 1 = 2 -> 50-2 = 48 -> 48/50 siendo 50 el tamaño del read
sub its_deletion
{
	my $tmp_deletion_number=$_[0];
	return ( $tmp_deletion_number * (48/50) );
}
