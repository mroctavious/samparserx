###################################################################################################################################################
#                                                                                                                                                 #
#  - GENOME READ                                                                                                                                  #
#                                                                                                                                                 #
#  - DESCRPCION:                                                                                                                                  #
#                                                                                                                                                 #
#    El programa recibe un genoma de referencia y una lista de lecturas indicando su posicion inicial en el genoma de referencia.                 #
#    Compara cada lectura con el genoma, buscando matches y missmatches por tipo de base y produce una tabla donde se indica la frecuencia de     #
#    aparicion de operacion (M o MM) por base para todo nucleotido del genoma. Un Match resta 1 a las frecuencias y un Missmatch suma 1.          #
#                                                                                                                                                 #
#  - RUN:                                                                                                                                         #
#                                                                                                                                                 #
#    python programa genoma reads 16300 tot_number_reads                                                                                          #
#                                                                                                                                                 #
###################################################################################################################################################
# CODE ############################################################################################################################################

# LIBRARIES #######################################################################################################################################

# system functions --------------------------------------------------------------------------------------------------------------------------------
from sys import argv, exit
from os import mkdir

# execution functions -----------------------------------------------------------------------------------------------------------------------------
from time import time
start_time = time()

# data structures functions -----------------------------------------------------------------------------------------------------------------------
from collections import defaultdict

# GLOBAL VARIABLES ################################################################################################################################

# input -------------------------------------------------------------------------------------------------------------------------------------------
REF_GENOME_FILE = argv[1]
READS_FILE = argv[2]
FILE_OUT = argv[4]
# array -------------------------------------------------------------------------------------------------------------------------------------------
GENOME = []               # Guarda el genoma de referencia.
A = []                    # Guarda la cantidad de matches, missmatches o  "0" del nucleotido A, crece a la par de C, G y T.
C = []                    # "" ~.
G = []                    # "" ~.
T = []                    # "" ~ Los arreglos A C G y T seran guardados como valores de la hash FRECUENCY con llaves: "A", "C", "G" y  "T"
                          # y tendran la misma longitud que el genoma.
TOT_READS = []            # guarda la cantidad de lecturas que tuvieron matches y missmatches para cada lugar en el genoma.
TOT_MATCHES_FREC = []     # arreglo que gurada la frecuencia relativa de matches por lugar en el genoma de referencia. Relativa respecto a la
                          # cantidad de lecturas que cayeron ahi.  
TOT_MISSMAT_FREC = []     # arreglo que guarda la frecuencia relativa del total de missmatches (los tres demas nucleotidos) por lugar en el genoma.
A_FREC = []               # guarda el negativo de la frecuencia relativa de matches o missmatches con A para cada lugar en el genoma.
C_FREC = []               # lo mismo para C
G_FREC = []               # lo mismo para G
T_FREC = []               # lo mismo para T
POSITION = []             # arreglo de posciones en el genoma

# hash --------------------------------------------------------------------------------------------------------------------------------------------
READS_M = defaultdict(list)                        # tendra llaves iguales a cada indice del genoma (iniciando en cero porque asi lo hace python)
                                                   # y  arreglos de arreglos como valores donde cada subarreglo es una lectura iniciada en llave
                                                   # con identificardo M (MATCH O MISSMATCH).
READS_X = defaultdict(list)                        # lecturas con identificador X (MISSMATCH).
READS_E = defaultdict(list)                        # lecturas con identificador = (EQUAL o MATCH).
FRECUENCY = dict.fromkeys(["A", "C", "G", "T"])    # sus valores seran los arreglos A, C, G y T.

# file --------------------------------------------------------------------------------------------------------------------------------------------
TIEMPO = None
TABLA = None

# MODIFICADORES DE EJECUCION ----------------------------------------------------------------------------------------------------------------------
CYCLIC_GENOME = int(argv[3])
MIN_SCORE = 0.01
if(not((CYCLIC_GENOME == -1) or (CYCLIC_GENOME > 0))):
    print("INVALID INPUT FOR GENOME LENGTH!!!")
    exit

# ESCALARES ---------------------------------------------------------------------------------------------------------------------------------------
#MAPPED = int(argv[4])
#FREC_COEF = ((1000000*16300) / (MAPPED*50))
A_NEW = []
C_NEW = []
G_NEW = []
T_NEW = []
MATCH_NEW = []

# FUNCTIONS #######################################################################################################################################

# funcion 1: capturar genoma de referencia --------------------------------------------------------------------------------------------------------
def GET_REFERENCE_GENOME():
    # variables
    global REF_GENOME_FILE                                   # el nombre del archivo que contiene al genoma de referencia.
    INPUT_FILE_1 = open(REF_GENOME_FILE, "r")    # manejador del archivo del genoma. 
    GENOME_AS_ARRAY = []                                     # arreglo donde se guardara cada caracter del genoma.
    # capturar el genoma en un arreglo
    for line in INPUT_FILE_1:
        GENOME_AS_ARRAY = list(line.strip())
    INPUT_FILE_1.close()
    # fin de funcion
    return(GENOME_AS_ARRAY)

# funcion 2: capturar lecturas en la hash ---------------------------------------------------------------------------------------------------------
def GET_READS():
    # variables
    global READS_FILE                                        # nombre del archivo de lecturas.
    global READS_M                                           # guarda las lecturas con M por lugar de inicio en el genoma.
    global READS_X                                           # guarda las lecturas con X por lugar de inicio en el genoma.
    global READS_E                                           # guarda las lecturas con = (EQUAL) por lugar de inicio en el genoma.
    INPUT_FILE_2 = open(str(READS_FILE), "r")         # manejador del archivo de lecturas.
    line = []                                                # cada linea del archivo como arreglo
    info = []                                                # localizacion mas M, x o =.
    location = 0                                             # indice de inicio de la lectura. Se debe reducir en uno.
    single_read = []                                         # cada lectura.
    # guardar las lecturas en hashes segun sea el caso M, X, o = (E)
    for each_line in INPUT_FILE_2:
        line = each_line.split(" | ")
        single_read = list(line[1].strip())
        if("M" in line[0]):
            info = line[0].split("M")
            location = int(info[0]) - 1
            single_read.append(int(info[1]))
            READS_M[location].append(single_read)
            continue
        if("X" in line[0]):
            info = line[0].split("X")
            location = int(info[0]) - 1
            single_read.append(int(info[1]))
            READS_X[location].append(single_read)
            continue
        if("=" in line[0]):
            info = line[0].split("=")
            location = int(info[0]) - 1
            single_read.append(int(info[1]))
            READS_E[location].append(single_read)
    INPUT_FILE_2.close()
    # fin de funcion

# funcion 3: llenado de la tabla de freciencias ---------------------------------------------------------------------------------------------------
def GET_FRECUENCIES(is_cyclic):
    # variables
    global GENOME                                                    # genoma de referencia.
    index = 0                                                        # el indice en el genoma.
    CURRENT_BASE = ""                                                # el nucleotido en el genoma.
    BASES = []                                                       # los nucleotidos de READS_M a comparar contra el del genoma.
    # conformacion de la tabla de frecuecnias con las lecturas de READS M al leer el genoma
    for index in range(len(GENOME)):
        CURRENT_BASE = GENOME[index]
        BASES = SEARCH_READS_M(index)
        COMPARE_M(CURRENT_BASE, BASES, index, is_cyclic)
    # suma de las frecuencias de missmatches
    SUM_X_FRECUENCIES(is_cyclic)
    # resta de las frecuencias de matches
    SUBSTRACT_E_FRECUENCIES(is_cyclic)
    # fin de la funcion
    
# funcion 4: manejo de datos de hash READS --------------------------------------------------------------------------------------------------------
def SEARCH_READS_M(position):
    # variables
    global READS_M                                                   # tabla de lecturas.
    CURRENT_READS = []                                               # valor de READS_M para la llave position.
    REPETITIONS= [0, 0, 0, 0]                                        # cantidad de lecturas que tuvieron cierto nucleotido [A, C, G, T].
    PARTIAL_TMP = []                                                 # lo que queda de cada lectura despues de quitar la primera letra
    INITIAL= ""                                                      # la inicial de cada lectura en CURRENT_READS
    each_read = 0                                                    # contador de las lecturas en CURRENT_READS
    next_location = position + 1                                     # siguiente posicion en el genoma y en las llaves de READS_M.
    # captura de las lecturas en la poscion especificada
    CURRENT_READS = READS_M[position]
    # tomar las iniciales y pasar los sobrantes al siguiente indice de READS
    for each_read in range(len(CURRENT_READS)):
        PARTIAL_TMP = CURRENT_READS[each_read]
        INITIAL = PARTIAL_TMP.pop(0)
        if(INITIAL == "A"):
            REPETITIONS[0] = REPETITIONS[0] + PARTIAL_TMP[len(PARTIAL_TMP)-1]
            if(len(PARTIAL_TMP) > 1):
                READS_M[next_location].append(PARTIAL_TMP)
            continue
        if(INITIAL == "C"):
            REPETITIONS[1] = REPETITIONS[1] + PARTIAL_TMP[len(PARTIAL_TMP)-1]
            if(len(PARTIAL_TMP) > 1):
                READS_M[next_location].append(PARTIAL_TMP)
            continue
        if(INITIAL == "G"):
            REPETITIONS[2] = REPETITIONS[2] + PARTIAL_TMP[len(PARTIAL_TMP)-1]
            if(len(PARTIAL_TMP) > 1):
                READS_M[next_location].append(PARTIAL_TMP)
            continue
        if(INITIAL == "T"):
            REPETITIONS[3] = REPETITIONS[3] + PARTIAL_TMP[len(PARTIAL_TMP)-1]
            if(len(PARTIAL_TMP) > 1):
                READS_M[next_location].append(PARTIAL_TMP)
        if((INITIAL == "N") and (len(PARTIAL_TMP) > 1)):
            READS_M[next_location].append(PARTIAL_TMP)
    # ya que guardamos los valores que necesitamos eliminamos la posicion de READS para liberar memoria
    READS_M.pop(position)
    # fin de la funcion 
    return(REPETITIONS)

# funcion 5: comparacion de nucleotidos -----------------------------------------------------------------------------------------------------------
def COMPARE_M(ref, frecuencies, loc, cyclic):
    # variables
    global GENOME
    global A                                                         # arreglo de frecuencias de A.
    global C                                                         # arreglo de frecuencias de C.
    global G                                                         # arreglo de frecuencias de G.
    global T                                                         # arreglo de frecuencias de T.
    cyc_pos = 0                                                      # posicion a modificar si el genoma es ciclico
    # multiplicar por menos uno el contador correspondiente a los matches y agregar cada frecuencia a su correspondiente arreglo
    # version con genoma ciclico
    if((loc >= cyclic) and (cyclic < len(GENOME))):
        cyc_pos = loc - cyclic
        if(ref == "A"):
            A[cyc_pos] = A[cyc_pos]  + ((-1)*frecuencies[0])
            C[cyc_pos] = C[cyc_pos] + (frecuencies[1])
            G[cyc_pos] = G[cyc_pos] + (frecuencies[2])
            T[cyc_pos] = T[cyc_pos] + (frecuencies[3])
            return
        if(ref == "C"): 
            C[cyc_pos] = C[cyc_pos]  + ((-1)*frecuencies[1])
            A[cyc_pos] = A[cyc_pos] + (frecuencies[0])
            G[cyc_pos] = G[cyc_pos] + (frecuencies[2])
            T[cyc_pos] = T[cyc_pos] + (frecuencies[3])
            return
        if(ref == "G"):
            G[cyc_pos] = G[cyc_pos]  + ((-1)*frecuencies[2])
            C[cyc_pos] = C[cyc_pos] + (frecuencies[1])
            A[cyc_pos] = A[cyc_pos] + (frecuencies[0])
            T[cyc_pos] = T[cyc_pos] + (frecuencies[3])
            return
        if(ref == "T"):
            T[cyc_pos] = T[cyc_pos]  + ((-1)*frecuencies[3])
            C[cyc_pos] = C[cyc_pos] + (frecuencies[1])
            G[cyc_pos] = G[cyc_pos] + (frecuencies[2])
            A[cyc_pos] = A[cyc_pos] + (frecuencies[0])
            return
    # version con genoma no ciclico
    if(ref == "A"):
        A.append((-1)*frecuencies[0])
        C.append(frecuencies[1])
        G.append(frecuencies[2])
        T.append(frecuencies[3])
        return
    if(ref == "C"): 
        A.append(frecuencies[0])
        C.append((-1)*frecuencies[1])
        G.append(frecuencies[2])
        T.append(frecuencies[3])
        return
    if(ref == "G"):
        A.append(frecuencies[0])
        C.append(frecuencies[1])
        G.append((-1)*frecuencies[2])
        T.append(frecuencies[3])
        return
    if(ref == "T"):
        A.append(frecuencies[0])
        C.append(frecuencies[1])
        G.append(frecuencies[2])
        T.append((-1)*frecuencies[3])
        return
    # fin de la funcion
    
# funcion 6: sumar frecuencias de missmatches o X -------------------------------------------------------------------------------------------------
def SUM_X_FRECUENCIES(cicle):
    # variables
    global READS_X                                                   # lecturas de missmatches.
    global A                                                         # arregglo de frecuencias de A.
    global C                                                         # arregglo de frecuencias de C.
    global G                                                         # arregglo de frecuencias de G.
    global T                                                         # arregglo de frecuencias de T.
    POSITIONS_X = []                                                 # llaves de READS_X.
    each_position_x = 0                                              # contador sobre POSITIONS_X.
    CURRENT_READS_X = []                                             # cada grupo de lecturas X por posicione en el genoma.
    EACH_READ_X = []                                                 # cada lectura en CURRENT_READS_X.
    each_read_x = 0                                                  # contador sobre CURRENT_READS_X.
    each_base_x = ""                                                 # contador sobre EACH_READ_X.
    initial_pos_x = 0                                                # posicion inicial de cada lectura en el genoma.
    pos_x = 0                                                        # contador sobre el genoma.
    change_pos = 0                                                   # posicion a modificar.
    # obtener llaves de la hash READS_X
    POSITIONS_X = list(READS_X.keys())
    # agregar las frecuencias de missmatches
    for each_position_x in range(len(POSITIONS_X)):
        initial_pos_x = POSITIONS_X[each_position_x]
        CURRENT_READS_X = READS_X[initial_pos_x]
        for each_read_x in range(len(CURRENT_READS_X)):
            EACH_READ_X = CURRENT_READS_X[each_read_x]
            pos_x = initial_pos_x
            while(len(EACH_READ_X) > 1):
                each_base_x = EACH_READ_X.pop(0)
                change_pos = pos_x
                if(pos_x >= cicle):
                    change_pos = pos_x - cicle
                if(each_base_x == "A"):
                    A[change_pos] = A[change_pos] + EACH_READ_X[len(EACH_READ_X)-1]
                    pos_x = pos_x + 1
                    continue
                if(each_base_x == "C"):
                    C[change_pos] = C[change_pos] + EACH_READ_X[len(EACH_READ_X)-1]
                    pos_x = pos_x + 1
                    continue
                if(each_base_x == "G"):
                    G[change_pos] = G[change_pos] + EACH_READ_X[len(EACH_READ_X)-1]
                    pos_x = pos_x + 1
                    continue
                if(each_base_x == "T"):
                    T[change_pos] = T[change_pos] + EACH_READ_X[len(EACH_READ_X)-1]
                    pos_x = pos_x + 1
                    continue
                if(each_base_x == "N"):
                    pos_x = pos_x + 1
    # fin de la funcion

# funcion 7: restar frecuencias de matches o = (E) ------------------------------------------------------------------------------------------------
def SUBSTRACT_E_FRECUENCIES(cicle):
    # variables
    global READS_E                                                   # lecturas de missmatches.
    global A                                                         # arregglo de frecuencias de A.
    global C                                                         # arregglo de frecuencias de C.
    global G                                                         # arregglo de frecuencias de G.
    global T                                                         # arregglo de frecuencias de T.
    POSITIONS_E = []                                                 # llaves de READS_X.
    each_position_e = 0                                              # contador sobre POSITIONS_X.
    CURRENT_READS_E = []                                             # cada grupo de lecturas X por posicione en el genoma.
    EACH_READ_E = []                                                 # cada lectura en CURRENT_READS_X.
    each_read_e = 0                                                  # contador sobre CURRENT_READS_X.
    each_base_e = ""                                                 # contador sobre EACH_READ_X.
    initial_pos_e = 0                                                # posicion inicial de cada lectura en el genoma.
    pos_e = 0                                                        # contador sobre el genoma.
    change_pos = 0                                                   # posicion a modificar.
    # obtener llaves de la hash READS_X
    POSITIONS_E = list(READS_E.keys())
    # agregar las frecuencias de missmatches
    for each_position_e in range(len(POSITIONS_E)):
        initial_pos_e = POSITIONS_E[each_position_e]
        CURRENT_READS_E = READS_E[initial_pos_e]
        for each_read_e in range(len(CURRENT_READS_E)):
            EACH_READ_E = CURRENT_READS_E[each_read_e]
            pos_e = initial_pos_e
            while(len(EACH_READ_E) > 1):
                each_base_e = EACH_READ_E.pop(0)
                change_pos = pos_e
                if(pos_e >= cicle):
                    change_pos = pos_e - cicle
                if(each_base_e == "A"):
                    A[change_pos] = A[change_pos] + EACH_READ_E[len(EACH_READ_E)-1]
                    pos_e = pos_e + 1
                    continue
                if(each_base_e == "C"):
                    C[change_pos] = C[change_pos] + EACH_READ_E[len(EACH_READ_E)-1]
                    pos_e = pos_e + 1
                    continue
                if(each_base_e == "G"):
                    G[change_pos] = G[change_pos] + EACH_READ_E[len(EACH_READ_E)-1]
                    pos_e = pos_e + 1
                    continue
                if(each_base_e == "T"):
                    T[change_pos] = T[change_pos] + EACH_READ_E[len(EACH_READ_E)-1]
                    pos_e = pos_e + 1
                    continue
                if(each_base_e == "N"):
                    pos_e = pos_e + 1
    # fin de la funcion
                    
# funcion 8: calculo de frecuencias relativas de matches y missmatches para cada lugar en el genoma -----------------------------------------------
def GET_RELATIVE_FRECUENCIES(cicle_pos):
    # variables
    global GENOME
    global A
    global C
    global G
    global T
    global TOT_READS
    global TOT_MATCHES_FREC
    global TOT_MISSMAT_FREC
    global A_FREC
    global C_FREC
    global G_FREC
    global T_FREC
    global FREC_COEF
    mark = 0                                                  # contador sobre la longitud del genoma de referencia.
    reference = ""                                            # nucleotido en esa posicion.
    tot_reads = 0                                             # cantidad de lecturas que cayeron en ese lugar del genoma.
    A_val = 0                                                 # valor en A
    C_val = 0                                                 # valor en C
    G_val = 0                                                 # valor en G
    T_val = 0                                                 # valor en T
    # iterar sobre las frecuencias A, C, G, T
    for mark in range(cicle_pos):
        reference = GENOME[mark]
        A_val = abs(A[mark])
        C_val = abs(C[mark])
        G_val = abs(G[mark])
        T_val = abs(T[mark])
        tot_reads = A_val + C_val + G_val + T_val
        TOT_READS.append(tot_reads)
        if(tot_reads == 0):
            TOT_MATCHES_FREC.append(0)
            TOT_MISSMAT_FREC.append(0)
            A_FREC.append(0)
            C_FREC.append(0)
            G_FREC.append(0)
            T_FREC.append(0)
            continue
        if(reference == "A"):
            TOT_MATCHES_FREC.append(round(A_val*1000000/tot_reads, 2))
            TOT_MISSMAT_FREC.append(round(((C_val + G_val + T_val)*1000000/tot_reads), 2))
            A_FREC.append(round((-A_val*1000000/tot_reads), 2))
            C_FREC.append(round(C_val*1000000/tot_reads, 2))
            G_FREC.append(round(G_val*1000000/tot_reads, 2))
            T_FREC.append(round(T_val*1000000/tot_reads, 2))
            continue
        if(reference == "C"):
            TOT_MATCHES_FREC.append(round(C_val*1000000/tot_reads, 2))
            TOT_MISSMAT_FREC.append(round(((A_val + G_val + T_val)*1000000/tot_reads), 2))
            C_FREC.append(round((-C_val*1000000/tot_reads), 2))
            A_FREC.append(round(A_val*1000000/tot_reads, 2))
            G_FREC.append(round(G_val*1000000/tot_reads, 2))
            T_FREC.append(round(T_val*1000000/tot_reads, 2))
            continue
        if(reference == "G"):
            TOT_MATCHES_FREC.append(round(G_val*1000000/tot_reads, 2))
            TOT_MISSMAT_FREC.append(round(((C_val + A_val + T_val)*1000000/tot_reads), 2))
            G_FREC.append(round((-G_val*1000000/tot_reads), 2))
            C_FREC.append(round(C_val*1000000/tot_reads, 2))
            A_FREC.append(round(A_val*1000000/tot_reads, 2))
            T_FREC.append(round(T_val*1000000/tot_reads, 2))
            continue
        if(reference == "T"):
            TOT_MATCHES_FREC.append(round(T_val*1000000/tot_reads, 2))
            TOT_MISSMAT_FREC.append(round(((C_val + G_val + A_val)*1000000/tot_reads), 2))
            T_FREC.append(round((-T_val*1000000/tot_reads), 2))
            C_FREC.append(round(C_val*1000000/tot_reads, 2))
            G_FREC.append(round(G_val*1000000/tot_reads, 2))
            A_FREC.append(round(A_val*1000000/tot_reads, 2))

# MAIN ############################################################################################################################################

# guardar el genoma de referencia.
GENOME = GET_REFERENCE_GENOME()

# guarda las lecturas en READS.
GET_READS()

# verificar si el genoma es ciclico o no
if(CYCLIC_GENOME == -1):
    CYCLIC_GENOME = len(GENOME)
if(CYCLIC_GENOME > len(GENOME)):
    print("INVALID INPUT FOR GENOME LENCTH!!!")
    exit

# construir los arreglos de frecuencias
GET_FRECUENCIES(CYCLIC_GENOME)

# crear columnas de frecuencias relativas
GET_RELATIVE_FRECUENCIES(CYCLIC_GENOME)

# generar arreglo de posciones del genoma
POSITION = list(range(1, CYCLIC_GENOME + 1))

# construimos una tabla con la informacion
FRECUENCY["A"] = A                                                   
FRECUENCY["C"] = C
FRECUENCY["G"] = G
FRECUENCY["T"] = T

# imprimmir resultados
TABLA = open(FILE_OUT, "w")

TABLA.write("LOCUS\tGENOME\tA\tC\tG\tT\tREADS\tMATCH\tMISS\tA_FREC\tC_FREC\tG_FREC\tT_FREC\n")

for i in range(CYCLIC_GENOME):
    #if(TOT_MISSMAT_FREC[i] < MIN_SCORE):
    #    continue
    TABLA.write(str(POSITION[i])+"\t"+str(GENOME[i])+"\t"+str(A[i])+"\t"+str(C[i])+"\t"+str(G[i])+"\t"+str(T[i])+"\t"+str(TOT_READS[i])+"\t"+str(TOT_MATCHES_FREC[i])+"\t"+str(TOT_MISSMAT_FREC[i])+"\t"+str(A_FREC[i])+"\t"+str(C_FREC[i])+"\t"+str(G_FREC[i])+"\t"+str(T_FREC[i])+"\n")

TABLA.close()

# imprimir tiempo de ejecucion
#TIEMPO = open(FILE_OUT, "w")
#TIEMPO.write(str(time() - start_time) + " segundos")
#TIEMPO.close()

# END #############################################################################################################################################
###################################################################################################################################################

