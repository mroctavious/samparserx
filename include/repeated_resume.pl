#!/usr/bin/perl

use strict;
use 5.010;
use warnings;
use threads;
use threads::shared;
use Thread::Queue qw( );
use lib qw(include);
use Formulas;

##Argumentos
my @files=@ARGV;
my $number_of_del=pop(@files);
my $number_files=@files;
my %rp_count;
$rp_count{'Total'}=0;
sub hash_add
{
	my $key=$_[0];
	my $n=$_[1];
	if(exists($rp_count{$key}))
	{
		$rp_count{$key}+=$n;
	}
	else
	{
		$rp_count{$key}=$n;
	}
	$rp_count{'Total'}+=$n;

}
##Contar por tamano de repeated deletion
foreach(@files)
{
	open my $F_entrada, '<', $_;
	##Va analizar linea por linea
	while(<$F_entrada>)
	{
		chomp;
		##Para saltar cualquier comentario que empieze con #
		if($_ =~ /^\s*[A-Z]/)
		{
			next;
		}
		my @rd_len = split "=>", $_;
		#say "$rd_len[0] -- $rd_len[1]";
		my $value=$rd_len[1];
		hash_add($rd_len[0],$value );

	}

}
say "LENGTH\t#FOUND";
my @freq_number;
my @freq_total;
my @freq_multiply;

my $i=0;
foreach my $len(sort keys %rp_count)
{
	say "$len\t$rp_count{$len}";
	my $num= $rp_count{$len}/$number_of_del;
	my $tot= $rp_count{$len}/$rp_count{'Total'};
	my $multy= $rp_count{$len}/($number_of_del * $number_files);
	if ( $len =~ /^\d+?$/)
	{
		$freq_number[$len]="$len\t$num\t$rp_count{$len}  / ".$number_of_del;
		$freq_total[$len]="$len\t$tot\t$rp_count{$len}  / ".$rp_count{'Total'};
		$freq_multiply[$len]="$len\t$multy\t$rp_count{$len}  / ".$number_of_del*$number_files;
	}
	
}
say "\n\nFreq with deletions made per file";
say "LENGTH\tVALUE";
foreach(@freq_number)
{
	if($_)
	{
		say;
	}
}
say "\n\nFreq with deletions made per file and multiply it with by the number of files";
say "LENGTH\tVALUE";
foreach(@freq_multiply)
{
	if($_)
	{
		say;
	}
}
say "\n\nFreq with the number of total Repeated Deletions Found";
say "LENGTH\tVALUE";

foreach(@freq_total)
{
	if($_)
	{
		say;
	}
}
