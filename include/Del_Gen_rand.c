#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <syscall.h>

//Output file name and ptr
FILE *outfile;

//Array which all thread will have access to
int *start_arr=NULL;
int *end_arr=NULL;

//How many threads we are going to send
#define threads_number 32

int calcular_fin(int,int);

//Recursive function which will return the random number inside the given range
int random_interval(int min, int max)
{
	//Create random number
	int num=rand();

	//Check if its inside the range given
	if(num >= min && num <= max)
	{
		//If its inside rnge then return it
		return num;
	}
	else
	{
		return random_interval(min, max);
	}

}


//Non Recursive function which will return the random number inside the given range
int random_interval_iter(int min, int max)
{
	//iterate while flag is turned off
	int flag=0;
	int num;
	while(flag==0)
	{
		//Create random number
		num=rand();
		//Check if the number is within the range
		if(num >= min && num <= max)
		{
			flag=1;
		}
	}
	return num;
}



//Non Recursive function which will return the random number inside the given range
int random_interval_mod(int min, int max, int seed)
{
	int mod=max;
	//Create random number
        //int num=rand_r(&seed);
        int num=rand();
	int return_num=num%mod;
	if(return_num==0)
	{
		return_num=1;
	}
	return return_num;
}


//Return the number within the min and max range
int random_interval_mod_length(int min, int max, int seed)
{
	int mod=max;
	int flag=0;
	int return_num;
	while(flag==0)
	{
		//Create random number
	        //int num=rand_r(&seed);
	        int num=rand();
		return_num=num%mod;
		if(return_num >= min && return_num <= max)
		{
			flag=1;
		}
	}
	return return_num;
}



struct random_thrd_args
{
	int veces;
	int min;
	int max;
	int genome;
	unsigned int seed;
};


void *random_interval_thrd(void *args)
{
	//Call the Data Structure made for the arguments of the threads
	struct random_thrd_args *args_struct=args;

	//Veces its the number of lines will create the thread
	int veces=args_struct->veces;

	//The minimum splitread length
	int min=args_struct->min;

	//The minimum splitread length
	int max=args_struct->max;

	//The genome length
	int genome_length=args_struct->genome;

	//Recover the seed for the random number generator
	unsigned int seed=args_struct->seed;

	//The tid of the threads are untrackable for me at this moment
	//Get thread identifier number
	//pid_t tid = syscall(__NR_gettid);
	//printf("before calling pthread_create getpid: %d getpthread_self: %lu tid:%lu\n",getpid(), pthread_self(), syscall(SYS_gettid));
	//Start to create lines
	int i=0;
	while(i<veces)
	{

		//Get the starting pos from recursive function
		int pos=random_interval_mod(1, genome_length,seed+i);

		//How many positions it will go forward
		int avance=random_interval_mod_length(min,max,seed+i);

		//The ending position of the splitread
		int pos_final=calcular_fin(pos,avance);

		//Print on file
		fprintf(outfile,"%d | %d | RD | %d\n",pos, pos_final, avance);
		//printf("%d | %d | RD | %d | i=%d\n",pos, pos_final, avance,i);
		i++;
	}
	return 0;
}


//CPU Sequential
int random_interval_gen(int veces, int min, int max, int genome)
{
	int i=1;
	while(i<=veces)
	{

		//Get the starting pos from recursive function
		int pos=random_interval_mod(1, genome,i*i-i);

		//How many positions it will go forward
		int avance=random_interval_mod_length(min,max,i*i-1);

		//The ending position of the splitread
		int pos_final=calcular_fin(pos,avance);

		//Print on file
		fprintf(outfile,"%d | %d | RD | %d\n",pos, pos_final, avance);
		//printf("%d | %d | RD | %d | i=%d\n",pos, pos_final, avance,i);
		i++;
	}
	return 0;
}


//This function will the end position of the deletion
int calcular_fin(int init_pos, int avance)
{
	int pos_final=init_pos+avance;
	if( pos_final > 16300 )
	{
		return pos_final-16300;
	}
	else
	{
		return pos_final;
	}
}


//Main program
int main (int argc, char *argv[])
{
	//Create new random seed
	srand((unsigned) time(NULL));

	//Create seed
	unsigned int seed=((unsigned) time(NULL));

	//i its the counting variable for the while
	int i=0;

	//How many deletions the program will create
	int veces=atoi(argv[1]);

	//The minimum length of the splitreads
	int min=atoi(argv[2]);

	//Max length of the splitreads
	int max=atoi(argv[3]);

	//The genome length
	int genome_length=atoi(argv[4]);

	//Create structure for the args for each thread
	struct random_thrd_args thrd_args;

	//We create the TID variable
	pthread_t tid;

	//Now we open the file for writing
	outfile=fopen(argv[5], "w" );

	//Print Header in file
	fprintf(outfile,"@Random_Deletion=%d\n",veces);

	//How many lines per thread the program will send to each thread
	//int lines_per_thread=(veces/threads_number)+1;

	//int lines_per_thread=(veces/threads_number)+threads_number;
	int lines_per_thread=(veces/threads_number);
	//Now the program set the values for the structure
	thrd_args.veces=lines_per_thread;
	thrd_args.min=min;
	thrd_args.max=max;
	thrd_args.genome=genome_length;
	thrd_args.seed=seed;

	//Reserve the memory of the arrays
	start_arr=malloc(sizeof(int)*(veces*threads_number));
	end_arr=malloc(sizeof(int)*(veces*threads_number));

	//int thrd=thrd_args.veces;
	//int mmin=thrd_args.min;
	//int mmax=thrd_args.max;
	//int gen=thrd_args.genome;
	//printf("Veces %d VS %d\nMin %d VS %d\nMax %d VS %d\nGenome %d VS %d\n",lines_per_thread,thrd,min,mmin,max,mmax,genome_length,gen);
/*
	//Start to take time for the parallel solution
	clock_t timer1 = clock();
	//The program will iterate the times given from the argv (veces)
	while(i<threads_number)
	{
		pthread_create(&tid, NULL, random_interval_thrd, (void *)&thrd_args);
		//Next thread
		i++;
	}
	pthread_join(tid, NULL);
	timer1 = clock() - timer1;
	printf("Operacion en Paralelo toma %10.3f ms.\n", (((float)timer1) / CLOCKS_PER_SEC) * 1000);
*/

	//Sequential

	//Start to take time for the sequential solution
	clock_t timer2 = clock();

	//Call the sequential function
	random_interval_gen(veces,min,max,genome_length);
	timer2 = clock() - timer2;
	printf("Operacion en CPU toma %10.3f ms.\n", (((float)timer2) / CLOCKS_PER_SEC) * 1000);

	//pthread_join(tid, NULL);
	fclose(outfile);
	return 0;
}

